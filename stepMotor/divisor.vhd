library ieee;
use ieee.std_logic_1164.all;

entity divisor is
port (
	fclk50MHz: in std_logic;
	fclk100Hz: out std_logic
);
end entity divisor;
    
architecture behavior of divisor is
    signal cnt: integer := 0;
	 signal clk: std_logic := '0';
begin
	process(fclk50Mhz) is
	begin
		if rising_edge(fclk50Mhz) then
			if cnt = 2 then
			--if cnt = 100000 then
				cnt <= 0;
				clk <= not clk;
			else 
				cnt <= cnt + 1;
			end if;
		end if;
	end process;    
end architecture behavior;  