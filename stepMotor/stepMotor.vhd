library ieee;
use ieee.std_logic_1164.all;

entity stepMotor is
port (
	fclk50MHz: in std_logic;
	dir: in std_logic;
	A,B,C,D : out std_logic
);
end entity stepMotor;
    
architecture behavior of stepMotor is
    signal lines: std_logic_vector(3 downto 0);
	 signal fclk100Hz: std_logic := '0';

begin
    --lines <= A&B&C&D;
	 A<=lines(3);
	 B<=lines(2);
	 C<=lines(1);
	 D<=lines(0);
	 u1: entity work.divisor(behavior) port map(fclk50MHz,fclk100Hz);
	 u2: entity work.controlador(behavior) port map(fclk100Hz,dir,lines);
end architecture behavior;  