library ieee;
use ieee.std_logic_1164.all;

entity controlador is
port (
	fclk100Hz: in std_logic;
	dir: in std_logic;
	lines : out std_logic_vector(3 downto 0)
);
end entity controlador;
    
architecture behavior of controlador is
	type states is (e0,e1,e2,e3);
	signal state: states := e0;
begin
	process(fclk100Hz) is
	begin
		if rising_edge(fclk100Hz) then
			case state is
				when e0 => lines <= "1100";
					if dir = '0' then
						state <= e1;
					else 
						state <= e3;
					end if;
				when e1 => lines <= "0110";
					if dir = '0' then
						state <= e2;
					else 
						state <= e0;
					end if;
				when e2 => lines <= "0011";
					if dir = '0' then
						state <= e3;
					else 
						state <= e1;
					end if;
				when e3 => lines <= "1001";
					if dir = '0' then
						state <= e1;
					else 
						state <= e2;
					end if;
			end case;		
		end if;
	end process;    
end architecture behavior;  