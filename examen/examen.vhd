library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
entity examen is
port (
	clk: in std_logic;
	d : in std_logic;
	word: out std_logic_vector(7 downto 0) ;
	rxrdy: out std_logic := '0'
);
end entity examen;
    
architecture behavior of examen is
	type states is (e0, e1);
	signal state : states := e0;
	signal contador : integer := 0;

begin
	process(clk)
	begin 
		if rising_edge(clk) then
			case state is
				when e0 =>
					word(contador) <= d;
					contador <= contador + 1;
					if contador = 8 then 
						state <= e1;
					end if;
				when e1 =>
					rxrdy <= '1';
			end case;
		end if;
   end process;
end architecture behavior; 