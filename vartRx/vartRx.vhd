library ieee;
use ieee.std_logic_1164.all;

entity vartRx is
port (
	rx: in std_logic;
	tick: in std_logic;
	data: out std_logic_vector(7 downto 0);
    rx_done: out std_logic
);
end entity vartRx;
    
architecture behavior of vartRx is
    signal rxData: std_logic_vector(7 downto 0) := x'00';
	--signal done : std_logic := '0';
	signal s: integer range 0 to 15 := 0; 	-- contador de ticks
	signal n: integer range 0 t0 7 := 0;	-- contador de bits
	type states(idle,start);
	signal state: states := idle;
begin
    data <= rxData;
	rx_donde <= done;
	process(tick)
    begin 
        if rising_edge(tick) then
            case state is
					when idle =>
                    if	rx = '0' then
                        rx_done <= '0';
                        s <= 0;
                        state <= start;
                    end if;    
               when start =>
                    if s = 7 then
                        s <= 0
                        n <= 0
                        state <= data;
                    else
                        s <= s + 1;
							end if;
               when data => 
							if s = 15
								rxData(n) <= rx;
								s <= 0;
								if n = 7 then
									state <= stop;
								else
									n <= n +1 ;
								end if;
							else
								s <= s + 1;
					when stop =>
						if s = 15 then
							rx_done <= '1';
						else
							s <= s + 1;
						end if;
            end case;            
        end if;
    end process;
end architecture behavior;  