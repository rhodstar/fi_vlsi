library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
entity sumador is
port (
	clk: in std_logic;
	m: in std_logic;
	s: in std_logic;
	d: out std_logic
);
end entity sumador;
    
architecture behavior of sumador is
	type states is (e0, e1);
	signal state : states := e0;
	signal ms: std_logic_vector(1 downto 0);

begin
	process(clk)
    begin 
		  ms <= m&s;
        if rising_edge(clk) then
			  case state is
				when e0 => 
					if ms = "01" then
						d <= '1';
					end if;
					if ms = "10" then 
						d <= '1';
					end if;
					if ms = "00" then
						d <= '0';
					else
						d <= '0';
						state <= e1;
					end if;	
					
				when e1 => 
					if ms = "01" then
						d <= '0';
					end if;
					if ms = "10" then 
						d <= '0';
					end if;
					if ms = "11" then
						d <= '1';
					else
						d <= '1';
						state <= e0;
					end if;	
				end case;
        end if;
    end process;
end architecture behavior;  