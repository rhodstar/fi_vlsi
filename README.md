# VLSI

Plantilla general para la creación de archivos vhdl

```vhdl
library ieee;
use ieee.std_logic_1164.all;

entity lampara is
port (
	clk: in std_logic;
	switch: in std_logic;
	lamp: out std_logic = '0'
);
end entity lampara;
    
architecture behavior of lampara is
    signal luz: std_logic := '0';


begin
    lamp <= luz;
	process(clk)
    begin 
        if rising_edge(clk) then
        
        end if;
    end process;
end architecture behavior;  
```

## Problema 1

```vhdl
library ieee;
use ieee.std_logic_1164.all;

entity lampara is
port (
	clk: in std_logic;
	boton: in std_logic;
	lamp: out std_logic
);
end entity lampara;

architecture behavior of lampara is
	signal luz: std_logic :='0';
begin
	lamp <= luz;
	process(clk)
	begin
		if rising_edge(clk) then
			if boton = '1' then
				luz <= not luz;
			end if;
		end if;
	end process;
end architecture behavior;
```

## Problema 2

Diseñe una carta ASM para un circuito que enciende una lámpara cuando se activa un interrumptor de dos posiciones (encendido, apagado). La lámpara se apaga cuando el interrumptor se regresa a la posición de apagado. Considere que la lámpara esta inicialmente apagada.

[Agregar esquema(circuito)]

[Agregar un diagrama de bloques]

[Agregar carta ASM]

```vhdl
library ieee;
use ieee.std_logic_1164.all;

entity lampara is
port (
	clk: in std_logic;
	switch: in std_logic;
	lamp: out std_logic := '0'
);
end entity lampara;
    
architecture behavior of lampara is
    signal luz: std_logic := '0';
	-- Lo siguiente es similar a las enumeraciones
	type states is (sw_off, sw_on);
	signal state: states := sw_off;

begin
    lamp <= luz;
	process(clk)
    begin 
        if rising_edge(clk) then
        	case state is
            when sw_off => 
                if switch = '1' then
                	luz <= '1';
                	state <= sw_on;
            	end if;
        	when sw_on =>
                    if switch = '0' then
                        luz <= '0';
                    	state <= sw_off;
            		end if;
			end case;		
        end if;
    end process;
end architecture behavior;                 
```

## Problema 3

Un sistema checador de paridad (par de unos), el sistema debe generar una salida para el bit que ingrese. Si el número de 1's es par la salida se enciende. En otro caso se apaga.

```vhdl
library ieee;
use ieee.std_logic_1164.all;

entity paridad is
port (
	clk: in std_logic;
	bitIn: in std_logic;
	par: out std_logic := '0'
);
end entity paridad;
    
architecture behavior of paridad is
    type states is (e0,non,par1);
	 signal state: states := e0;

begin
    
	process(clk)
    begin 
        if rising_edge(clk) then
			case state is
				when e0 => 
					if bitIn = '1' then
						state <= non;
					end if;
				when non => 
					par <= '0';
					if bitIn = '1' then
						state <= par1;
					else 
						state <= non;
					end if;
				when par1 =>
					par <= '1';
					if	bitIn = '1' then
						state <= non;
					end if;
				end case;
        end if;
    end process;
end architecture behavior; 
```

## Problema 4

Diseñe un contador de 10 a 15

```vhdl
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
entity contador is
port (
	clk: in std_logic;
	salida : out std_logic_vector(3 downto 0)
);
end entity contador;
    
architecture behavior of contador is
	type states is (e0, e1);
	signal state : states := e0;
	signal counter: std_logic_vector(3 downto 0) := "0000";
begin

	process(clk)
	begin 
		salida <= counter;
		if rising_edge(clk) then
			counter <= counter + "0001";
			if counter = "1111" then
				salida <= counter;
				counter <= "0000";
			end if;	
		end if;  
	end process;
end architecture behavior; 
```

## Problema 5

Design and implement a traffic light control

Restriccions:

* The green light remains 20 s
* The yellow ligth remains 10 s
* The red ligth remains 30 s

And more, you are free to make any design and use a convenient time base as in your next quit, the ASM chart must match the code.

```vhdl
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
entity semaforo is
port (
	clk: in std_logic;
	green_out : out std_logic;
	yellow_out : out std_logic;
	red_out : out std_logic
);
end entity semaforo;
    
architecture behavior of semaforo is
	type states is (e0, yellow, red, green);
	signal state : states := e0;
	signal counter: std_logic_vector(5 downto 0) := "000000";
begin

	process(clk)
	begin 
		if rising_edge(clk) then
			case state is
			when e0 => counter <= "000000";
				state <= yellow;
			when yellow => yellow_out <= '1';
				counter <= counter + "000001";
				if counter = "001010" then
					yellow_out <= '0';
					state <= red;
				end if;
			when red => red_out <= '1';
				counter <= counter + "000001";
				if counter = "101000" then 
					red_out <= '0';
					state <= green;
				end if;
			when green => green_out <= '1';
				counter <= counter + "000001";
				if counter = "111100" then
					green_out <= '0';
					state <= e0;
				end if;
			end case;
		end if;  
	end process;
end architecture behavior; 
```

## Problema 6

Sumador

```vhdl
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
entity sumador is
port (
	clk: in std_logic;
	m: in std_logic;
	s: in std_logic;
	d: out std_logic
);
end entity sumador;
    
architecture behavior of sumador is
	type states is (e0, e1);
	signal state : states := e0;
	signal ms: std_logic_vector(1 downto 0);

begin
	process(clk)
    begin 
		  ms <= m&s;
        if rising_edge(clk) then
			  case state is
				when e0 => 
					if ms = "01" then
						d <= '1';
					end if;
					if ms = "10" then 
						d <= '1';
					end if;
					if ms = "00" then
						d <= '0';
					else
						d <= '0';
						state <= e1;
					end if;	
					
				when e1 => 
					if ms = "01" then
						d <= '0';
					end if;
					if ms = "10" then 
						d <= '0';
					end if;
					if ms = "11" then
						d <= '1';
					else
						d <= '1';
						state <= e0;
					end if;	
				end case;
        end if;
    end process;
end architecture behavior;  
```

## Problema 7

Thre is an array stated as:

​	`signal xbuff: td_logic_vector(7 downto 0) := "10110011";`

It's desirable to transmit each bit of the array in serial form and using two lines:

1) One line for data

2) Second line for slave-clock

Thre is a third line but it is ground.

```vhdl
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
entity buffering is
port (
	clk: in std_logic;
	slave_clk: out std_logic;
	salida : out std_logic
);
end entity buffering;
    
architecture behavior of buffering is
	type states is (e0, e1,e2);
	signal state : states := e0;
	signal contador : integer := 0;
	signal data: std_logic_vector(7 downto 0):= "10110011";

begin
	process(clk)
	begin 
		if rising_edge(clk) then
			case state is
				when e0 =>
					salida <= data(contador);
					slave_clk <= '0';
					state <= e1;
				when e1 =>
					slave_clk <= '1';
					if contador = 7 then 
						state <= e2;
					else 
						contador <= contador + 1;
						state <= e0;
					end if;
				when e2 =>
					salida <= '0';
			end case;
		end if;
   end process;
end architecture behavior; 
```

## Guía examen

### a) Martes 

fpga -> driver -> servomotor

### b) Lunes: Examen

* Fabricantes de CI
* Personajes que intervienen en el desarollo del transistor
* Ventajas de los sistemas embebidos
* Empresas fabricantes de FPGA
  * Xilinx
  * Intel (Altera)
* Empresas fabricantes de DSK con 
  * FPGA: Spartan, Terasic, National Instruments

* Software para desarollo de programas
  * VHDL: quartus, ISE Xilinx, Vivado (Xilinx)

## Problema 8

Design a synchronous receiver for your synchronous transmiter

Restrictions

* The receiver must capture 8 bits 
* Your system must show the received word in an eight pins port
* There is a RxRdy terminal: This terminal turns on when a complete word has been received.

```vhdl
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
entity receptor is
port (
	clk: in std_logic;
	d : in std_logic;
	word: out std_logic_vector(7 downto 0) ;
	rxrdy: out std_logic := '0'
);
end entity receptor;
    
architecture behavior of receptor is
	type states is (e0, e1);
	signal state : states := e0;
	signal contador : integer := 0;

begin
	process(clk)
	begin 
		if rising_edge(clk) then
			case state is
				when e0 =>
					word(contador) <= d;
					contador <= contador + 1;
					if contador = 8 then 
						state <= e1;
					end if;
				when e1 =>
					rxrdy <= '1';
			end case;
		end if;
   end process;
end architecture behavior; 
```

## Proyecto 1 (ServoMotor)

## Proyecto 2 (Stepper Motor)

## Proyecto 3 (UART)

```vhdl
library ieee;
use ieee.std_logic_1164.all;

entity vartRx is
port (
	rx: in std_logic;
	tick: in std_logic;
	data: out std_logic_vector(7 downto 0);
    rx_done: out std_logic
);
end entity vartRx;
    
architecture behavior of vartRx is
    signal rxData: std_logic_vector(7 downto 0) := x'00';
	--signal done : std_logic := '0';
	signal s: integer range 0 to 15 := 0; 	-- contador de ticks
	signal n: integer range 0 t0 7 := 0;	-- contador de bits
	type states(idle,start);
	signal state: states := idle;
begin
    data <= rxData;
	rx_donde <= done;
	process(tick)
    begin 
        if rising_edge(tick) then
            case state is
				when idle =>
                    if	rx = '0' then
                        rx_done <= '0';
                        s <= 0;
                        state <= start;
                    end if;    
                when start =>
                    if s = 7 then
                        s <= 0
                        n <= 0
                        state <= data;
                    else
                        s <= s + 1;
                when data => 
                        
            end case;            
        end if;
    end process;
end architecture behavior;  
```



