library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
entity semaforo is
port (
	clk: in std_logic;
	green_out : out std_logic;
	yellow_out : out std_logic;
	red_out : out std_logic
);
end entity semaforo;
    
architecture behavior of semaforo is
	type states is (e0, yellow, red, green);
	signal state : states := e0;
	signal counter: std_logic_vector(5 downto 0) := "000000";
begin

	process(clk)
	begin 
		if rising_edge(clk) then
			case state is
			when e0 => counter <= "000000";
				state <= yellow;
			when yellow => yellow_out <= '1';
				counter <= counter + "000001";
				if counter = "001010" then
					yellow_out <= '0';
					state <= red;
				end if;
			when red => red_out <= '1';
				counter <= counter + "000001";
				if counter = "101000" then 
					red_out <= '0';
					state <= green;
				end if;
			when green => green_out <= '1';
				counter <= counter + "000001";
				if counter = "111100" then
					green_out <= '0';
					state <= e0;
				end if;
			end case;
		end if;  
	end process;
end architecture behavior; 