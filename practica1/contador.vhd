library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
entity contador is
port (
	clk: in std_logic;
	salida : out std_logic_vector(3 downto 0)
);
end entity contador;
    
architecture behavior of contador is
	type states is (e0, e1);
	signal state : states := e0;
	signal counter: std_logic_vector(3 downto 0) := "0000";
begin

	process(clk)
	begin 
		if rising_edge(clk) then
			case state is
			when e0 => counter <= "0000";
				state <= e1;
			when e1 => salida <= counter;
				counter <= counter + "0001";
				if counter = "1111" then
					salida <= counter;
					state <= e0;
				end if;
			end case;
		end if;  
	end process;
end architecture behavior; 