library ieee;
use ieee.std_logic_1164.all;

entity registersFile is
port (clk:	in	std_logic;

	Rw		:	in	std_logic_vector (3 downto 0);
	LdRw	:	in	std_logic;

	Rx		:	in	std_logic_vector (2 downto 0);
	Ry		:	in	std_logic_vector (2 downto 0);
	Rz		:	in	std_logic_Vector (2 downto 0);
				
	F_alu	:	in	std_logic_Vector (7 downto 0);
	F_RAM	:	in	std_logic_Vector (7 downto 0);
	F_cst8:	in	std_logic_Vector (7 downto 0);
	Rv		:	in	std_logic_vector (1 downto 0);
			
	Qx		:	out	std_logic_Vector (7 downto 0);
	Qy		:	out	std_logic_Vector (7 downto 0);
	Qz		:	out	std_logic_Vector (7 downto 0);
			
	PC		:	out	std_logic_Vector (7 downto 0);
	
	xR1,xR2,xR3 :	out	std_logic_Vector (7 downto 0)
);

end entity registersFile;

architecture behavior of registersFile is
	signal	F				:	std_logic_Vector (7 downto 0);
	signal	Q0 ,Q1 ,Q2, Q3, Q4, Q5, Q6, Q7	:	std_logic_vector (7 downto 0);
	signal	Ld0,Ld1,Ld2,Ld3,Ld4,Ld5,Ld6,Ld7	:	std_logic;
begin
	r0	:	entity work.register8 (behavior) port map (clk,Ld0, F ,Q0 );
	r1	:	entity work.register8 (behavior) port map (clk,Ld1, F ,Q1 );
	r2	:	entity work.register8 (behavior) port map (clk,Ld2, F ,Q2 );
	r3	:	entity work.register8 (behavior) port map (clk,Ld3, F ,Q3 );
	r4	:	entity work.register8 (behavior) port map (clk,Ld4, F ,Q4 );
	r5	:	entity work.register8 (behavior) port map (clk,Ld5, F ,Q5 );
	r6	:	entity work.register8 (behavior) port map (clk,Ld6, F ,Q6 );
	r7	:	entity work.register8 (behavior) port map (clk,Ld7, F ,Q7 ); --Program Counter
	
	--Program counter
	PC<=Q7;
	
	--Rw selector
	ld0<=LdRw when Rw="0000" else '0';
	ld1<=LdRw when Rw="0001" else '0';
	ld2<=LdRw when Rw="0010" else '0';
	ld3<=LdRw when Rw="0011" else '0';
	ld4<=LdRw when Rw="0100" else '0';
	ld5<=LdRw when Rw="0101" else '0';
	ld6<=LdRw when Rw="0110" else '0';
	ld7<=LdRw when Rw="0111" else '0';
	
	--Qx selector
	with Rx select
		Qx <=	Q0 when "000",
			Q1 when "001",
			Q2 when "010",
			Q3 when "011",
			Q4 when "100",
			Q5 when "101",
			Q6 when "110",
			Q7 when "111",
			
			Q0 when others;
			
	--Qy selector
	with Ry select
		Qy <=	Q0 when "000",
			Q1 when "001",
			Q2 when "010",
			Q3 when "011",
			Q4 when "100",
			Q5 when "101",
			Q6 when "110",
			Q7 when "111",
			
			Q0 when others;

	--Qz selector
	with Rz select
		Qz <=	Q0 when "000",
			Q1 when "001",
			Q2 when "010",
			Q3 when "011",
			Q4 when "100",
			Q5 when "101",
			Q6 when "110",
			Q7 when "111",
			
			Q0 when others;
	
	--Result selector
	with Rv select
		F <=	F_alu		when "00",
			F_RAM		when "01",
			F_cst8		when "10",
			x"00"		when others;
			
	xR1<=Q1;
	xR2<=Q2;
	xR3<=Q3;

end architecture behavior;

