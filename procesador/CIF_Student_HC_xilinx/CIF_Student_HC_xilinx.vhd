library ieee;
use ieee.std_logic_1164.all;

entity CIF_Student_HC is
port (
	clk50Mhz	:	in	std_logic;
	led		:	out	std_logic:='0'; --This led is a monitor to check de internal clock frquency
	leds : out	std_logic_vector ( 7 downto 0) -- Leds to monitor the output
  );
end entity CIF_Student_HC;


architecture behavior of CIF_Student_HC is
	signal	clk		: std_logic;
	signal	PC			: std_logic_vector ( 7 downto 0);
	signal	instruction	: std_logic_vector (16 downto 0);
	
	--signal Rw			:	std_logic_vector ( 2 downto 0); It isn't necessary
	signal	LdRw		:	std_logic;

	signal	Rx			:	std_logic_vector (3 downto 0);
	signal	Ry			:	std_logic_vector (3 downto 0);
	signal	Rz			:	std_logic_Vector (3 downto 0);
	signal	Rv			:	std_logic_vector (1 downto 0);

	signal	F_alu		:	std_logic_vector (7 downto 0);
	signal	F_RAM		:	std_logic_vector (7 downto 0);
	signal	F_cst8	:	std_logic_vector (7 downto 0);

	signal	Qx,Qy,Qz	:	std_logic_vector (7 downto 0);
	
	signal	LdIR		:	std_logic;
	signal	opCode	: 	std_logic_vector ( 4 downto 0);
	signal	op1		:	std_logic_vector ( 3 downto 0);
	signal	op2		:	std_logic_vector ( 3 downto 0);
	signal	op3		:	std_logic_Vector ( 3 downto 0);

	signal	ops	:	std_logic_Vector ( 4 downto 0);
	
	-- RAM --
	signal	DM_addr	:	std_logic_vector ( 7 downto 0);
	signal	DM_RW		:	std_logic;
	signal	DM_D		:	std_logic_Vector ( 7 downto 0);
	signal	DM_W		:	std_logic_vector ( 7 downto 0);
	
	-- Multiplexers --
	signal	op1Sel	:	std_logic;
	signal	op3Sel	:	std_logic;
	
	-- Monitoring --
	signal xR1,xR2,xR3	:	std_logic_vector ( 7 downto 0);

begin

	led<=clk;

	--divfrq: entity work.divisor(behavior) port map (clk50Mhz,clk);
	--clk50Mhz: externa clock of 50Mhz. Pin_P11
	--clk: internal clock of 1Hz: indicate here which pin was used to monitor the "clk" signal.
	clk <= clk50Mhz; -- estamos puenteando la señal

	progMem: entity work.programMemory (behavior) port map (PC, instruction);
	
	--PC : program counter
	--instruction is a bus for the instruction recovered from program memory
	
	rf:entity work.registersFile (behavior) port map (	clk,

								Rz,
								LdRw,

								Rx(2 downto 0),
								Ry(2 downto 0),
								Rz(2 downto 0),
																			
								F_alu,
								F_RAM,
								F_cst8,
								Rv,
																		
								Qx,
								Qy,
								Qz,

								PC, 
								
								xR1,xR2,xR3 
							);
							
	ir: entity work.instructionregister(behavior) port map (clk, LdIR, instruction, opCode, op1, op2, op3, F_cst8);
	
	al : entity work.alu(behavior) port map (Qx,Qy,ops,F_alu);
	
	dataMem: entity work.RAM (behavior) port map(	clk,
							DM_addr,
							DM_RW,
							DM_D,
							DM_W
						);
	
	Rx <= op1 when op1Sel='0' else x"7";
	Ry <= op2;
	Rz <= op3 when op3Sel='0' else x"7";
	
	ctrl: entity work.controlUnit(behavior) port map(	clk,
							opCode,
							ops,
							LdIR,
							LdRw,
							DM_RW,
							Rv,
							op1Sel,
							op3Sel
						);
						
	-- Uncomment one of the following 3 lines to monitor a variable
	leds<=xR1;
	--leds<=xR2;
	--leds<=xR3;

end architecture behavior; 