/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0xfbc00daa */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "/data/semestre-08/vlsi/repositorio/procesador/CIF_Student_HC_xilinx/alu.vhd";
extern char *IEEE_P_0774719531;
extern char *IEEE_P_2592010699;

char *ieee_p_0774719531_sub_1496620905533649268_2162500114(char *, char *, char *, char *, char *, char *);
char *ieee_p_0774719531_sub_1496620905533721142_2162500114(char *, char *, char *, char *, char *, char *);
char *ieee_p_0774719531_sub_2255506239096166994_2162500114(char *, char *, char *, char *, int );
char *ieee_p_2592010699_sub_16439767405979520975_503743352(char *, char *, char *, char *, char *, char *);
char *ieee_p_2592010699_sub_16439989832805790689_503743352(char *, char *, char *, char *, char *, char *);
char *ieee_p_2592010699_sub_16439989833707593767_503743352(char *, char *, char *, char *, char *, char *);
char *ieee_p_2592010699_sub_207919886985903570_503743352(char *, char *, char *, char *);


static void work_a_0832606739_2372691052_p_0(char *t0)
{
    char t34[16];
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    int t5;
    char *t6;
    char *t7;
    int t8;
    char *t9;
    char *t10;
    int t11;
    char *t12;
    char *t13;
    int t14;
    char *t15;
    char *t16;
    int t17;
    char *t18;
    int t20;
    char *t21;
    int t23;
    char *t24;
    int t26;
    char *t27;
    char *t29;
    char *t30;
    char *t31;
    char *t32;
    char *t33;
    unsigned int t35;
    unsigned int t36;
    unsigned char t37;

LAB0:    t1 = (t0 + 2784U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(20, ng0);
    t2 = (t0 + 1352U);
    t3 = *((char **)t2);
    t2 = (t0 + 5157);
    t5 = xsi_mem_cmp(t2, t3, 5U);
    if (t5 == 1)
        goto LAB5;

LAB14:    t6 = (t0 + 5162);
    t8 = xsi_mem_cmp(t6, t3, 5U);
    if (t8 == 1)
        goto LAB6;

LAB15:    t9 = (t0 + 5167);
    t11 = xsi_mem_cmp(t9, t3, 5U);
    if (t11 == 1)
        goto LAB7;

LAB16:    t12 = (t0 + 5172);
    t14 = xsi_mem_cmp(t12, t3, 5U);
    if (t14 == 1)
        goto LAB8;

LAB17:    t15 = (t0 + 5177);
    t17 = xsi_mem_cmp(t15, t3, 5U);
    if (t17 == 1)
        goto LAB9;

LAB18:    t18 = (t0 + 5182);
    t20 = xsi_mem_cmp(t18, t3, 5U);
    if (t20 == 1)
        goto LAB10;

LAB19:    t21 = (t0 + 5187);
    t23 = xsi_mem_cmp(t21, t3, 5U);
    if (t23 == 1)
        goto LAB11;

LAB20:    t24 = (t0 + 5192);
    t26 = xsi_mem_cmp(t24, t3, 5U);
    if (t26 == 1)
        goto LAB12;

LAB21:
LAB13:    xsi_set_current_line(21, ng0);
    t2 = (t0 + 5205);
    t4 = (t0 + 3184);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    t9 = (t7 + 56U);
    t10 = *((char **)t9);
    memcpy(t10, t2, 8U);
    xsi_driver_first_trans_fast_port(t4);

LAB4:    xsi_set_current_line(20, ng0);

LAB39:    t2 = (t0 + 3104);
    *((int *)t2) = 1;
    *((char **)t1) = &&LAB40;

LAB1:    return;
LAB5:    xsi_set_current_line(21, ng0);
    t27 = (t0 + 5197);
    t29 = (t0 + 3184);
    t30 = (t29 + 56U);
    t31 = *((char **)t30);
    t32 = (t31 + 56U);
    t33 = *((char **)t32);
    memcpy(t33, t27, 8U);
    xsi_driver_first_trans_fast_port(t29);
    goto LAB4;

LAB6:    xsi_set_current_line(21, ng0);
    t2 = (t0 + 1032U);
    t3 = *((char **)t2);
    t2 = (t0 + 5064U);
    t4 = (t0 + 1192U);
    t6 = *((char **)t4);
    t4 = (t0 + 5080U);
    t7 = ieee_p_0774719531_sub_1496620905533649268_2162500114(IEEE_P_0774719531, t34, t3, t2, t6, t4);
    t9 = (t34 + 12U);
    t35 = *((unsigned int *)t9);
    t36 = (1U * t35);
    t37 = (8U != t36);
    if (t37 == 1)
        goto LAB23;

LAB24:    t10 = (t0 + 3184);
    t12 = (t10 + 56U);
    t13 = *((char **)t12);
    t15 = (t13 + 56U);
    t16 = *((char **)t15);
    memcpy(t16, t7, 8U);
    xsi_driver_first_trans_fast_port(t10);
    goto LAB4;

LAB7:    xsi_set_current_line(21, ng0);
    t2 = (t0 + 1032U);
    t3 = *((char **)t2);
    t2 = (t0 + 5064U);
    t4 = (t0 + 1192U);
    t6 = *((char **)t4);
    t4 = (t0 + 5080U);
    t7 = ieee_p_0774719531_sub_1496620905533721142_2162500114(IEEE_P_0774719531, t34, t3, t2, t6, t4);
    t9 = (t34 + 12U);
    t35 = *((unsigned int *)t9);
    t36 = (1U * t35);
    t37 = (8U != t36);
    if (t37 == 1)
        goto LAB25;

LAB26:    t10 = (t0 + 3184);
    t12 = (t10 + 56U);
    t13 = *((char **)t12);
    t15 = (t13 + 56U);
    t16 = *((char **)t15);
    memcpy(t16, t7, 8U);
    xsi_driver_first_trans_fast_port(t10);
    goto LAB4;

LAB8:    xsi_set_current_line(21, ng0);
    t2 = (t0 + 1032U);
    t3 = *((char **)t2);
    t2 = (t0 + 5064U);
    t4 = ieee_p_0774719531_sub_2255506239096166994_2162500114(IEEE_P_0774719531, t34, t3, t2, 1);
    t6 = (t34 + 12U);
    t35 = *((unsigned int *)t6);
    t36 = (1U * t35);
    t37 = (8U != t36);
    if (t37 == 1)
        goto LAB27;

LAB28:    t7 = (t0 + 3184);
    t9 = (t7 + 56U);
    t10 = *((char **)t9);
    t12 = (t10 + 56U);
    t13 = *((char **)t12);
    memcpy(t13, t4, 8U);
    xsi_driver_first_trans_fast_port(t7);
    goto LAB4;

LAB9:    xsi_set_current_line(21, ng0);
    t2 = (t0 + 1032U);
    t3 = *((char **)t2);
    t2 = (t0 + 5064U);
    t4 = (t0 + 1192U);
    t6 = *((char **)t4);
    t4 = (t0 + 5080U);
    t7 = ieee_p_2592010699_sub_16439989832805790689_503743352(IEEE_P_2592010699, t34, t3, t2, t6, t4);
    t9 = (t34 + 12U);
    t35 = *((unsigned int *)t9);
    t36 = (1U * t35);
    t37 = (8U != t36);
    if (t37 == 1)
        goto LAB29;

LAB30:    t10 = (t0 + 3184);
    t12 = (t10 + 56U);
    t13 = *((char **)t12);
    t15 = (t13 + 56U);
    t16 = *((char **)t15);
    memcpy(t16, t7, 8U);
    xsi_driver_first_trans_fast_port(t10);
    goto LAB4;

LAB10:    xsi_set_current_line(21, ng0);
    t2 = (t0 + 1032U);
    t3 = *((char **)t2);
    t2 = (t0 + 5064U);
    t4 = (t0 + 1192U);
    t6 = *((char **)t4);
    t4 = (t0 + 5080U);
    t7 = ieee_p_2592010699_sub_16439767405979520975_503743352(IEEE_P_2592010699, t34, t3, t2, t6, t4);
    t9 = (t34 + 12U);
    t35 = *((unsigned int *)t9);
    t36 = (1U * t35);
    t37 = (8U != t36);
    if (t37 == 1)
        goto LAB31;

LAB32:    t10 = (t0 + 3184);
    t12 = (t10 + 56U);
    t13 = *((char **)t12);
    t15 = (t13 + 56U);
    t16 = *((char **)t15);
    memcpy(t16, t7, 8U);
    xsi_driver_first_trans_fast_port(t10);
    goto LAB4;

LAB11:    xsi_set_current_line(21, ng0);
    t2 = (t0 + 1032U);
    t3 = *((char **)t2);
    t2 = (t0 + 5064U);
    t4 = (t0 + 1192U);
    t6 = *((char **)t4);
    t4 = (t0 + 5080U);
    t7 = ieee_p_2592010699_sub_16439989833707593767_503743352(IEEE_P_2592010699, t34, t3, t2, t6, t4);
    t9 = (t34 + 12U);
    t35 = *((unsigned int *)t9);
    t36 = (1U * t35);
    t37 = (8U != t36);
    if (t37 == 1)
        goto LAB33;

LAB34:    t10 = (t0 + 3184);
    t12 = (t10 + 56U);
    t13 = *((char **)t12);
    t15 = (t13 + 56U);
    t16 = *((char **)t15);
    memcpy(t16, t7, 8U);
    xsi_driver_first_trans_fast_port(t10);
    goto LAB4;

LAB12:    xsi_set_current_line(21, ng0);
    t2 = (t0 + 1032U);
    t3 = *((char **)t2);
    t2 = (t0 + 5064U);
    t4 = ieee_p_2592010699_sub_207919886985903570_503743352(IEEE_P_2592010699, t34, t3, t2);
    t6 = (t34 + 12U);
    t35 = *((unsigned int *)t6);
    t36 = (1U * t35);
    t37 = (8U != t36);
    if (t37 == 1)
        goto LAB35;

LAB36:    t7 = (t0 + 3184);
    t9 = (t7 + 56U);
    t10 = *((char **)t9);
    t12 = (t10 + 56U);
    t13 = *((char **)t12);
    memcpy(t13, t4, 8U);
    xsi_driver_first_trans_fast_port(t7);
    goto LAB4;

LAB22:;
LAB23:    xsi_size_not_matching(8U, t36, 0);
    goto LAB24;

LAB25:    xsi_size_not_matching(8U, t36, 0);
    goto LAB26;

LAB27:    xsi_size_not_matching(8U, t36, 0);
    goto LAB28;

LAB29:    xsi_size_not_matching(8U, t36, 0);
    goto LAB30;

LAB31:    xsi_size_not_matching(8U, t36, 0);
    goto LAB32;

LAB33:    xsi_size_not_matching(8U, t36, 0);
    goto LAB34;

LAB35:    xsi_size_not_matching(8U, t36, 0);
    goto LAB36;

LAB37:    t3 = (t0 + 3104);
    *((int *)t3) = 0;
    goto LAB2;

LAB38:    goto LAB37;

LAB40:    goto LAB38;

}


extern void work_a_0832606739_2372691052_init()
{
	static char *pe[] = {(void *)work_a_0832606739_2372691052_p_0};
	xsi_register_didat("work_a_0832606739_2372691052", "isim/CIF_Student_HC_isim_beh.exe.sim/work/a_0832606739_2372691052.didat");
	xsi_register_executes(pe);
}
