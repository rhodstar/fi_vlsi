library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity instructionRegister is
port (	clk		:	in 	std_logic;
	LdIR		:	in	std_logic;
	D		:	in	std_logic_vector (16 downto 0);
	opCode		: 	out	std_logic_vector ( 4 downto 0);
	op1		:	out	std_logic_vector ( 3 downto 0);
	op2		:	out	std_logic_vector ( 3 downto 0);
	op3		:	out	std_logic_vector ( 3 downto 0);
	cst8		:	out	std_logic_vector ( 7 downto 0)
);
end entity instructionRegister;

architecture behavior of instructionRegister is
	signal R : std_logic_Vector (16 downto 0):=(others=>'0');
begin

	process (clk)
	begin
		if clk'event and clk='1' then
			if LdIR='1' then
				R<=D;
			end if;
		end if;
	end process;

	opCode	<= R(16 downto 12);
	op1	<= R(11 downto  8); 
	op2	<= R( 7 downto  4); 
	op3	<= R( 3 downto  0); 
	cst8	<= R(11 downto 4);


end architecture behavior;