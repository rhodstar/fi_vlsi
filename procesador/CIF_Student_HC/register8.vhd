library ieee;
use ieee.std_logic_1164.all;

entity register8 is
port (	clk	:	in	std_logic;
	LdR	:	in	std_logic;
	D	:	in	std_logic_Vector (7 downto 0);
	Q	:	out	std_logic_Vector (7 downto 0)
		);
end entity register8;

architecture behavior of register8 is
	signal R : std_logic_vector (7 downto 0):=x"00";
begin

	Q<=R;

	process (clk)
	begin
		if clk'event and clk='1' then
			if LdR='1' then
				R<=D;
			end if;	
		end if;
	end process;
end architecture behavior;