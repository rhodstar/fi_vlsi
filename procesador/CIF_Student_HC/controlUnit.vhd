library ieee;
use ieee.std_logic_1164.all;

entity controlUnit is
generic (	NOP	:	std_logic_vector (4 downto 0):="00000";
			
				ADD	:	std_logic_vector (4 downto 0):="00001";
				SUB	:	std_logic_vector (4 downto 0):="00010";
				INC	:	std_logic_vector (4 downto 0):="00011";
				ANDop	:	std_logic_vector (4 downto 0):="00100";
				ORop	:	std_logic_vector (4 downto 0):="00101";
				XORop	:	std_logic_vector (4 downto 0):="00110";
				NOTop	:	std_logic_vector (4 downto 0):="00111";
						
				MUL	:	std_logic_vector (4 downto 0):="01001";
						
				CMPEQ	:	std_logic_vector (4 downto 0):="10001";				
				CMPGT	:	std_logic_vector (4 downto 0):="10010";				
				CMPLT	:	std_logic_vector (4 downto 0):="10011";				
				MVCL	:	std_logic_vector (4 downto 0):="10100";
				MVCH	:	std_logic_vector (4 downto 0):="10101";
				B		:	std_logic_vector (4 downto 0):="10110";
				CB		:	std_logic_vector (4 downto 0):="10111";
						
				STM	:	std_logic_vector (4 downto 0):="11001";
				LDM	:	std_logic_vector (4 downto 0):="11010"
);


port (	clk		:	in	std_logic;
			opCode	:	in	std_logic_vector (4 downto 0); 
			ops	:	out	std_logic_vector (4 downto 0):="00000";
			LdIR	:	out	std_logic:='0';
			LdRw	:	out	std_logic:='0';	--Registers Enable Write
			DMRW	:	out	std_logic:='0';	--Data RAM
			Rv	:	out	std_logic_vector (1 downto 0):="00";
			op1Sel	:	out	std_logic:='0';
			op3Sel	:	out	std_logic:='0'
	);

end entity controlUnit;


architecture behavior of controlUnit is
	type 		states 	is (fetch0,fetch1,fetch2,decode0,execMVCL,execADD, execORop, execB);
	signal 	nxtstate : 	states:=fetch0;
	signal 	state 	: 	states:=fetch0;
begin

	process (state)
	begin
	case state is
		when fetch0	=>	op1Sel		<='1';		
					op3Sel		<='1';
					ops		<="00011";	--inc operation: icrement R7(PC)
					Rv		<="00";		--recipient record
					ldRw		<='1';		
					ldIr		<='0';
					nxtstate	<=fetch1;
									
		when fetch1	=>	op1Sel		<='0';
					op3Sel		<='0';	
					ldRw		<='0';
					LdIr		<='1';
					nxtstate	<=fetch2;

		when fetch2	=>	LdIr		<='0';
					nxtstate	<=decode0;
									
		when decode0	=>	case opCode is
						when NOP	=>	nxtstate <= fetch0;
						when MVCL	=>	nxtstate <= execMVCL;
						when ADD	=>	nxtstate <= execADD;
						when ORop => nxtstate <= execORop;
						when B => nxtstate <= execB;
						when others	=>	nxtstate <= fetch0;
					end case;

		when execMVCL	=>	Rv		<="10";
					LdRw		<='1';
					ops		<="00000";
					nxtstate	<=fetch0;
																		
									
		when execADD	=>	Rv		<="00";
					LdRw		<='1';
					ops		<="00001";
					nxtstate	<=fetch0;
					
		when execORop => Rv <= "00";
					LdRw 		<= '1';
					ops 		<= "00101";
					nxtstate <= fetch0;
					
		when execB => Rv 		<= "10";
					LdRw 		<= '1';
					ops 		<= "00000";
					op3Sel 	<= '1';
					nxtstate <= fetch1;
					
									
	end case;
	end process;


	process (clk)		--this process update Mealy's state 
	begin
		if clk'event and clk='1' then
			state<=nxtState;
		end if;
	end process;

end architecture behavior;

