library ieee;
use ieee.std_logic_1164.all;
--use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity programMemory is

	generic (	SizeMemory	:	integer :=256;
		
		--Registers						
		R0		:	std_logic_vector (3 downto 0):="0000";
		R1		:	std_logic_vector (3 downto 0):="0001";
		R2		:	std_logic_vector (3 downto 0):="0010";
		R3		:	std_logic_vector (3 downto 0):="0011";
		R4		:	std_logic_vector (3 downto 0):="0100";
		R5		:	std_logic_vector (3 downto 0):="0101";
		R6		:	std_logic_vector (3 downto 0):="0110";
		R7		:	std_logic_vector (3 downto 0):="0111";

		--Mnemonics				
		NOP		:	std_logic_vector (16 downto 0):=(others=>'0');			
				
		ADD		:	std_logic_vector (4 downto 0):="00001";
		SUB		:	std_logic_vector (4 downto 0):="00010";
		INC		:	std_logic_vector (4 downto 0):="00011";
		ANDop		:	std_logic_vector (4 downto 0):="00100";
		ORop		:	std_logic_vector (4 downto 0):="00101";
		XORop		:	std_logic_vector (4 downto 0):="00110";
		NOTop		:	std_logic_vector (4 downto 0):="00111";
				
		MUL		:	std_logic_vector (4 downto 0):="01001";
				
		CMPEQ		:	std_logic_vector (4 downto 0):="10001";				
		CMPGT		:	std_logic_vector (4 downto 0):="10010";				
		CMPLT		:	std_logic_vector (4 downto 0):="10011";				
		MVCL		:	std_logic_vector (4 downto 0):="10100";
		MVCH		:	std_logic_vector (4 downto 0):="10101";
		B			:	std_logic_vector (4 downto 0):="10110";
		CB			:	std_logic_vector (4 downto 0):="10111";
				
		STM		:	std_logic_vector (4 downto 0):="11001";
		LDM		:	std_logic_vector (4 downto 0):="11010"
	);
	port (	pc		:	in	std_logic_vector (7 downto 0);
		instruction	:	out	std_logic_vector (16 downto 0)
	);	
end entity programMemory;

architecture behavior of programMemory is
type matrix is array (0 to 7) of std_logic_vector (16 downto 0);
signal mem:matrix:=(	NOP,
			MVCL & x"02" & R1,
			MVCL & x"04" & R2,	
			ADD  & R1 & R2 & R3,
			 
			NOP,
			NOP,
			NOP,
			NOP);
begin

	instruction<= mem(to_integer(unsigned(pc)));

end architecture behavior;