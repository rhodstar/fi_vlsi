library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity programMemory is 

generic(
	RO: std_logic_vector(3 downto 0):="0000";
	R1: std_logic_vector(3 downto 0):="0001";
	R2: std_logic_vector(3 downto 0):="0010";
	R3: std_logic_vector(3 downto 0):="0011";
	R4: std_logic_vector(3 downto 0):="0100";
	R5: std_logic_vector(3 downto 0):="0101";
	R6: std_logic_vector(3 downto 0):="0110";
	R7: std_logic_vector(3 downto 0):="0111";
	NOP: std_logic_vector(16 downto 0):=(others=>'0');
	MUCL: std_logic_vector(4 downto 0):="10100";
	ADD: std_logic_vector(4 downto 0):="00001"
	);
port(
Adde: in std_logic_vector(7 downto 0);
w: out std_logic_vector(16 downto 0):=(others=>'0')
);

end entity programMemory;
architecture behavior of programMemory is
type matrix is array(0 to 7) of std_logic_vector(16 downto 0);

signal mem:matrix:=(
NOP,
MUCL & x"02" & R1,
MUCL & x"04" & R2,
ADD & R1 & R2 & R3,
NOP,
NOP,
NOP,
NOP);


begin
w<= mem(to_integer(unsigned(adde)));
end architecture behavior;




