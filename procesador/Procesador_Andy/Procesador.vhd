library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity Procesador is port(
	clk: in std_logic;
	Rw: in std_logic_vector (3 downto 0);
	Ld: in std_logic; ---En el diagrama es LdRW
	RV : in std_logic_vector(1 downto 0);
	Rx : in std_logic_vector(3 downto 0);
	Ry : in std_logic_vector(3 downto 0);
	Rz : in std_logic_vector(3 downto 0);
	F_cst8 : in std_logic_vector(7 downto 0);
	F_ALU : in std_logic_vector(7 downto 0);
	F_RAM : in std_logic_vector(7 downto 0);
	Qx : out  std_logic_vector(7 downto 0);
	Qy : out std_logic_vector(7 downto 0);
	Qz : out std_logic_vector(7 downto 0);
	PC :out std_logic_vector(7 downto 0)--siempre el ultimo
	);
end entity Procesador;
architecture behavior of Procesador is
	signal F:std_logic_vector (7 downto 0);
	signal Q0,Q1,Q2,Q3,Q4,Q5,Q6,Q7: std_logic_vector (7 downto 0);
	signal Ld0,Ld1,Ld2,Ld3,Ld4,Ld5,Ld6,Ld7: std_logic;
	
begin 
	r0: entity work.registro(behavior) port map(clk,Ld0,F,Q0);
	r1: entity work.registro(behavior) port map(clk,Ld1,F,Q1);
	r2: entity work.registro(behavior) port map(clk,Ld2,F,Q2);
	r3: entity work.registro(behavior) port map(clk,Ld3,F,Q3);
	r4: entity work.registro(behavior) port map(clk,Ld4,F,Q4);
	r5: entity work.registro(behavior) port map(clk,Ld5,F,Q5);
	r6: entity work.registro(behavior) port map(clk,Ld6,F,Q6);
	r7: entity work.registro(behavior) port map(clk,Ld7,F,Q7);
	
	Ld0<= Ld when Rw="0000" else '0';
	Ld1<= Ld when Rw="0001" else '0';
	Ld2<= Ld when Rw="0010" else '0';
	Ld3<= Ld when Rw="0011" else '0';
	Ld4<= Ld when Rw="0100" else '0';
	Ld5<= Ld when Rw="0101" else '0';
	Ld6<= Ld when Rw="0110" else '0';
	Ld7<= Ld when Rw="0111" else '0';
	
	PC<=Q7;
	---Posiblemente modificar a 3 bits
	---------Completar Qy y Qz
	with Rx select
	Qx <= Q0 when "0000",
			Q1 when "0001",
			Q2 when "0010",
			Q3 when "0011",
			Q4 when "0100",
			Q5 when "0101",
			Q6 when "0110",
			Q7 when "0111",
			x"00" when others;
			
	with Ry select
	Qy <= Q0 when "0000",
			Q1 when "0001",
			Q2 when "0010",
			Q3 when "0011",
			Q4 when "0100",
			Q5 when "0101",
			Q6 when "0110",
			Q7 when "0111",
			x"00" when others;
			
	with Rz select
	Qz <= Q0 when "0000",
			Q1 when "0001",
			Q2 when "0010",
			Q3 when "0011",
			Q4 when "0100",
			Q5 when "0101",
			Q6 when "0110",
			Q7 when "0111",
			x"00" when others;
			
	with Rv select
	F <=	F_ALU		when "00",
			F_RAM		when "01",
			F_cst8	when "10",
			x"00"		when others;

end architecture behavior;