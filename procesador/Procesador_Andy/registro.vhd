library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity registro is port(
	clk: in std_logic;
	Ld: in std_logic;--habilitador de carga
	D: in std_logic_vector(7 downto 0);
	Q: out std_logic_vector(7 downto 0)
	);
end entity registro;

architecture behavior of registro is
signal R:std_logic_vector(7 downto 0):=x"00";

begin 
	Q<=R;
	process(clk)
	begin
		if rising_edge(clk) then
			if Ld = '1' then
				R<=D;
			end if;
		end if;
	end process;
	
	
end architecture behavior;