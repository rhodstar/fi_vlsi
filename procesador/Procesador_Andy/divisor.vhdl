library ieee;
use ieee.std_logic_1164.all;

entity divisor is
port (clk50Mhz : in std_logic;
		clk	: inout std_logic:='0'
);
end entity divisor;

architecture behavior of divisor is
begin
	process (clk50Mhz)
	variable contador : integer:=0;
	begin
	   if rising_edge(clk50Mhz) then
			if contador<25000000 then
				contador := contador + 1;
			else
				contador:=0;
				clk<= not clk;
			end if;	
		end if;	
	end process;
end architecture behavior;