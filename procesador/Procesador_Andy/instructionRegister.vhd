library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity instructionRegister is 

port(
clk: in std_logic;
LdIR: in std_logic;
instruction: in std_logic_vector(16 downto 0);
op1:		 out std_logic_vector(3 downto 0);
op2:		 out std_logic_vector(3 downto 0);
op3:		 out std_logic_vector(3 downto 0);	
cst8:		 out std_logic_vector(7 downto 0);
mnemonic: out std_logic_vector(4 downto 0)
);
end entity instructionRegister; 

architecture behavior of instructionRegister is
signal R:std_logic_vector(16 downto 0) :=(others=>'0');
begin 
mnemonic<=R(16 downto 12);
op1<=R(11 downto 8);
op2<=R(7 downto 4);
op3<=R(3 downto 0);
cst8<=R(11 downto 4);


process(clk)
begin
	if rising_edge(clk) then
		if LdIR = '1' then
			R<=instruction;
		end if;
	end if;
end process;
end architecture behavior;
