library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
entity ProcesadorT is port(

	clk50Mhz: in std_logic;
	xPC : out std_logic_vector(7 downto 0)

);
end entity ProcesadorT;


architecture behavior of ProcesadorT is
signal clk:std_logic;
signal RV : std_logic_vector(1 downto 0);
signal Rw,Rx, Ry, Rz :std_logic_vector(3 downto 0);
signal op1, op2, op3: std_logic_vector(3 downto 0);
signal Qx,Qy,Qz : std_logic_vector(7 downto 0);
signal PC: std_logic_vector(7 downto 0);
signal instruction: std_logic_vector(16 downto 0);
signal mnemonic : std_logic_vector(4 downto 0); --- equivalente a opCode
signal LdIR: std_logic;
signal LdRW : std_logic;
signal DM_RW : std_logic;
signal op1_sel, op3_sel : std_logic;
signal ops : std_logic_vector(4 downto 0);

signal F_ALU : std_logic_vector(7 downto 0);
signal F_RAM : std_logic_vector(7 downto 0);
signal cst8: std_logic_vector(7 downto 0);


--signal DM_D	: std_logic_Vector ( 7 downto 0);--RAM 
--signal DM_W	: std_logic_vector ( 7 downto 0);--RAM x2

signal addr : std_logic_vector(7 downto 0);--- es DM_addr?

--signal Rw_RAM : std_logic

begin

xPC <= PC;	

u0:entity work.divisor(behavior) port map(clk50MHz, clk);
u1:entity work.Procesador(behavior) port map (clk,Rw,LdRW,RV, Rx, Ry, Rz,cst8,F_ALU,x"00", Qx, Qy, Qz, PC);
u5:entity work.ALU(behavior) port map(Qx, Qy, ops, F_ALU);
u2:entity work.programMemory(behavior) port map (PC, instruction);
u3:entity work.instructionRegister(behavior) port map (clk,LdIR,instruction, op1, op2, op3, cst8, mnemonic);


Rx<= op1 when op1_sel='0' else x"7";
RY<= op2; 
Rz<= op3 when op3_sel='0' else x"7";


u4:entity work.controlUnit(behavior) port map (clk ,mnemonic, LdIR, LdRW, DM_RW, RV, op1_sel, op3_sel, ops);
--u6:entity work.RAM(behavior) port map (clk, addr,Rw_RAM, Qz,F_RAM);
	


	

	

end architecture behavior;