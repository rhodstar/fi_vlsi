library ieee;
use ieee.std_logic_1164.all;

entity controlUnit is
generic(
NOP:  std_logic_vector(4 downto 0):="00000";
MUCL: std_logic_vector(4 downto 0):="10100";
ADD: std_logic_vector (4 downto 0):="00001"
);

port(
	clk : in std_logic;
	mNemonic : in std_logic_vector(4 downto 0);
	LdIR : out std_logic := '0';
	LdRw : out std_logic := '0';
	DM_RW : out std_logic := '0';
	RV : out std_logic_vector(1 downto 0) := "00";
	op1_sel : out std_logic := '0';
	op3_sel : out std_logic := '0';
	ops : out std_logic_vector(4 downto 0) := "00000"

);

end entity controlUnit;

architecture behavior of controlUnit is
  type states is(fetch0, fetch1, fetch2, decode0, execMUCL, execADD);
  signal nxtstate:states:=fetch0;
  signal state:states:=fetch0;

begin
  process(state)
  begin
  case state is
  
  when fetch0=> 
        op1_sel<='1';
        op3_sel<='1';
        ops<="00011";
        Rv<= "00";
        LdRw<='1';
        nxtstate<=fetch1;
  when fetch1=>
        op1_sel<='0';
        op3_sel<='0';
        LdRw<='0';
        LdIR<='1';
        nxtstate<= fetch2;
  when fetch2=>
        LdIR<='0';
		  nxtstate<=decode0;
  when decode0=>
		  if mNemonic = NOP then
				nxtstate <= fetch0;
		  elsif mNemonic = MUCL then
				nxtstate <= execMUCL;
		  elsif mNemonic = ADD then
				nxtstate <= execADD;
		  else 
				nxtstate <= fetch0;
		  end if;
  when execMUCL =>
		  RV <= "10";
		  LdRW <= '1' ;
		  ops <= "00000";
		  nxtstate <= fetch0;
  when execADD =>
		  RV <= "00";
		  LdRW <= '1';
		  ops <= "00001" ;
		  nxtstate <= fetch0;
		    
  end case;

  end process;

process (clk)
begin
  if rising_edge(clk) then
    state<=nxtstate;
  end if;
end process;
end architecture behavior;