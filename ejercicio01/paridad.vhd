library ieee;
use ieee.std_logic_1164.all;

entity paridad is
port (
	clk: in std_logic;
	bitIn: in std_logic;
	par: out std_logic := '0'
);
end entity paridad;
    
architecture behavior of paridad is
    type states is (e0,non,par1);
	 signal state: states := e0;

begin
    
	process(clk)
    begin 
        if rising_edge(clk) then
			case state is
				when e0 => 
					if bitIn = '1' then
						state <= non;
					end if;
				when non => 
					par <= '0';
					if bitIn = '1' then
						state <= par1;
					else 
						state <= non;
					end if;
				when par1 =>
					par <= '1';
					if	bitIn = '1' then
						state <= non;
					end if;
				end case;
        end if;
    end process;
end architecture behavior; 