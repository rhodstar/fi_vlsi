library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
entity controlServo is
port (
	clk50MHz: in std_logic;
	widt : in std_logic_vector(7 downto 0);
	pwm: out std_logic
);
end entity controlServo;
    
architecture behavior of controlServo is
	--type states is (e0, e1);
	signal clk: std_logic :='0';
	signal counter : std_logic_vector(7 downto 0) := x"00";
	
begin
	-- DIVISOR
	process(clk50MHz)
		variable ent: integer := 0;
	begin 
		if rising_edge(clk50MHz) then
			if ent < 3906 then
				ent := ent + 1;
			else
				ent:= 0;
				clk <= not clk;
			end if;
		end if;
   end process;
	
	-- El counter
	process(clk)
	begin
		if rising_edge(clk) then
			if counter = x"FF" then
				counter <= x"00";
			else
				counter <= counter + 1;
			end if;
		end if;
	end process;
	
	-- PWM
	process(counter,clk)
	begin 
		if counter < widt then
			pwm <= '1';
		else 
			pwm <= '0';
		end if;
	end process;
	
end architecture behavior; 