library ieee;
use ieee.std_logic_1164.all;

entity lampara is
port (
	clk: in std_logic;
	boton: in std_logic;
	lamp: out std_logic
);
end entity lampara;

architecture behavior of lampara is
	signal luz: std_logic :='0';
	type states is (e0,
begin
	lamp <= luz;
	process(clk)
	begin
		if rising_edge(clk) then
			if boton = '1' then
				luz <= not luz;
			end if;
		end if;
	end process;
end architecture behavior;